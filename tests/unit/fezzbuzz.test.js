const tong =require('../../src/fizzbuzz')
describe('fizzbuzz',()=>{
    it('lekkla nun 1',()=>{
        const number =1;
        const result =tong.fizzbuzz(number);
        expect(result).toBe(1);

    })
    it('lekkla nun 2',()=>{
        const number =2;
        const result =tong.fizzbuzz(number);
        expect(result).toBe(2);

    })
    it('lekkla nun 3',()=>{
        const number =3;
        const result =tong.fizzbuzz(number);
        expect(result).toContain('fizz');

    })
    it('lekkla nun 4',()=>{
        const number =4;
        const result =tong.fizzbuzz(number);
        expect(result).toBe(4);

    })
    it('lekkla nun 5',()=>{
        const number =5;
        const result =tong.fizzbuzz(number);
        expect(result).toContain('buzz');

    })
    it('lekkla nun 6',()=>{
        const number =6;
        const result =tong.fizzbuzz(number);
        expect(result).toContain('fizz');

    })
    it('lekkla nun 7',()=>{
        const number =7;
        const result =tong.fizzbuzz(number);
        expect(result).toBe(7);

    })
    it('lekkla nun 8',()=>{
        const number =8;
        const result =tong.fizzbuzz(number);
        expect(result).toBe(8);

    })
    it('lekkla nun 9',()=>{
        const number =9;
        const result =tong.fizzbuzz(number);
        expect(result).toContain('fizz');

    })
    it('lekkla nun 10',()=>{
        const number =10;
        const result =tong.fizzbuzz(number);
        expect(result).toContain('buzz');

    })
    it('lekkla nun 11',()=>{
        const number =11;
        const result =tong.fizzbuzz(number);
        expect(result).toBe(11);

    })
    it('lekkla nun 12',()=>{
        const number =12;
        const result =tong.fizzbuzz(number);
        expect(result).toContain('fizz');

    })
    it('lekkla nun 13',()=>{
        const number =13;
        const result =tong.fizzbuzz(number);
        expect(result).toBe(13);

    })
    it('lekkla nun 14',()=>{
        const number =14;
        const result =tong.fizzbuzz(number);
        expect(result).toBe(14);

    })
    it('lekkla nun 15',()=>{
        const number =15;
        const result =tong.fizzbuzz(number);
        expect(result).toContain('fizzbuzz');

    })
    it('lekkla nun 30',()=>{
        const number =30;
        const result =tong.fizzbuzz(number);
        expect(result).toBe('fizzbuzz');

    })
    it('lekkla nun 40',()=>{
        const number =40;
        const result =tong.fizzbuzz(number);
        expect(result).toBe('buzz');

    })
    it('lekkla nun 66',()=>{
        const number =66;
        const result =tong.fizzbuzz(number);
        expect(result).toBe('fizz');

    })
    it('lekkla nun 87',()=>{
        const number =87;
        const result =tong.fizzbuzz(number);
        expect(result).toBe('fizz');

    })
    it('lekkla nun 100',()=>{
        const number =100;
        const result =tong.fizzbuzz(number);
        expect(result).toBe('buzz');

    })
})